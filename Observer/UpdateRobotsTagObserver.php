<?php
declare(strict_types=1);

namespace SixBySix\Seo\Observer;

use Magento\Framework\Event\Observer;
use SixBySix\Seo\Api\Helper\ConfigHelperInterface;
use SixBySix\Seo\Api\Observer\UpdateRobotsTagObserverInterface;
use SixBySix\Seo\Helper\ConfigHelper;

class UpdateRobotsTagObserver implements UpdateRobotsTagObserverInterface
{
    /** @var ConfigHelperInterface */
    protected $configHelper;

    /** @var \Magento\Framework\View\Page\Config */
    protected $pageConfig;

    /** @var \Magento\LayeredNavigation\Block\Navigation\State */
    protected $stateFilter;

    public function __construct(
        ConfigHelperInterface $configHelper,
        \Magento\Framework\View\Page\Config $pageConfig,
        \Magento\LayeredNavigation\Block\Navigation\State $stateFilter
    ) {
        $this->configHelper = $configHelper;
        $this->pageConfig = $pageConfig;
        $this->stateFilter = $stateFilter;
    }

    public function execute(Observer $observer)
    {
        if ($this->isFilteredPage()) {
            $robots = $this->configHelper->getMetaRobotsTagForFilteredPage();
        } else {
            return $this;
        }

        $this->pageConfig->setRobots($robots);

        return $this;
    }

    protected function isFilteredPage()
    {
        return (
            $this->stateFilter->getActiveFilters()
        );
    }
}
