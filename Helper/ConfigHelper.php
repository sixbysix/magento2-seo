<?php
declare(strict_types=1);

namespace SixBySix\Seo\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

/**
 * Class ConfigHelper
 * @package SixBySix\Seo\Helper
 */
class ConfigHelper extends AbstractHelper implements \SixBySix\Seo\Api\Helper\ConfigHelperInterface
{
    /**
     * Get meta robots tag for filtered category listings
     *
     * @return string|null
     */
    public function getMetaRobotsTagForFilteredPage(): ?string
    {
        return $this->scopeConfig->getValue(
            self::XML_ROBOTS_META_TAG_FILTERED,
            ScopeInterface::SCOPE_STORE
        );
    }
}
