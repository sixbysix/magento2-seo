<?php
declare(strict_types=1);

namespace SixBySix\Seo\Api\Data\Config\Source;

/**
 * Interface MetaRobotsTagInterface
 */
interface MetaRobotsTagInterface extends \Magento\Framework\Data\OptionSourceInterface
{

}
