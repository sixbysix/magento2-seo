<?php
declare(strict_types=1);

namespace SixBySix\Seo\Api\Observer;

interface UpdateRobotsTagObserverInterface extends \Magento\Framework\Event\ObserverInterface
{
}
