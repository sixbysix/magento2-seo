<?php
declare(strict_types=1);

namespace SixBySix\Seo\Api\Helper;

/**
 * Interface ConfigHelperInterface
 * @package SixBySix\Seo\Api\Helper
 */
interface ConfigHelperInterface
{
    const XML_ROBOTS_META_TAG_FILTERED = 'catalog/seo/robots_filtered_category';

    /**
     * @return string|null
     */
    public function getMetaRobotsTagForFilteredPage(): ?string;
}
