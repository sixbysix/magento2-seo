<?php
declare(strict_types=1);

namespace SixBySix\Seo\Model\Config\Source;

use SixBySix\Seo\Api\Data\Config\Source\MetaRobotsTagInterface;

/**
 * Class MetaRobotsTag
 * @package SixBySix\Seo\Model\Config\Source
 */
class MetaRobotsTag implements MetaRobotsTagInterface
{
    const DEFAULT = '';
    const INDEX_FOLLOW = 'INDEX,FOLLOW';
    const INDEX_NOFOLLOW = 'INDEX,NOFOLLOW';
    const NOINDEX_FOLLOW = 'NOINDEX,FOLLOW';
    const NOINDEX_NOFOLLOW = 'NOINDEX,NOFOLLOW';

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray(): array
    {
        return [
            ['value' => self::DEFAULT, 'label' => __('Default')],
            ['value' => self::INDEX_FOLLOW, 'label' => self::INDEX_FOLLOW],
            ['value' => self::INDEX_NOFOLLOW, 'label' => self::INDEX_NOFOLLOW],
            ['value' => self::NOINDEX_FOLLOW, 'label' => self::NOINDEX_FOLLOW],
            ['value' => self::NOINDEX_NOFOLLOW, 'label' => self::NOINDEX_NOFOLLOW],
        ];
    }
}
